package no.uib.inf101.sample.model;

public record Person(String name, int age) { }
