package no.uib.inf101.sample.view;

import javax.swing.JFrame;
import javax.swing.JPanel;

import no.uib.inf101.sample.model.Person;
import no.uib.inf101.sample.model.PersonList;

public class ViewMain {

    public static void main(String[] args) {
        PersonList model = new PersonList();
        for (int i = 0; i < 10; i++) {
            model.addPerson(new Person("Adam", i));
            model.addPerson(new Person("Eva", i));
        }

        ViewMain view = new ViewMain(model);

        JFrame frame = new JFrame("MOCK");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(view.mainPanel);
        frame.pack();
        frame.setVisible(true);
    }
    
    private PersonList model;
    private JPanel mainPanel;
    private ViewListOfPeople viewListOfPeople;
    private ViewPerson viewPerson;
    private JPanel rightPanel;

    public ViewMain(PersonList model) {
        this.model = model;

        this.mainPanel = new JPanel();
        this.viewListOfPeople = new ViewListOfPeople(model);
        this.viewPerson = new ViewPerson(new Person("Foo", -1));

        this.rightPanel = new JPanel();
        this.mainPanel.add(this.viewListOfPeople.getMainPanel());
        this.mainPanel.add(this.rightPanel);

        this.rightPanel.add(viewPerson.getMainPanel());

    }
}
